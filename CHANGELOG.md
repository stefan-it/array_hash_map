# Changelog

## 1.0.5 (2016-10-xx)

* New `-fstack-protector-strong` option introduced (see b57e8f6e)

* AddressSanitizer support added (see 60b2596a)

## 1.0.4 (2016-09-27)

* Contribution and coding style chapters added (see 33dd5532 and 3545f752)

* Project was moved to GitLab.com see new [home](https://gitlab.com/stefan-it/array_hash_map)

## 1.0.3 (2016-09-05)

* Adds new `-Weffc++` option to `Makefile` (see 87cfd317)

* Various `-Weffc++` optimizations (see a0e4cc5a and 728b878f)

## 1.0.2 (2015-07-24)

* Introduction of various new `Makefile` targets (see 2c317fbc)

## 1.0.1 (2015-07-14)

* `basic_unittest` is re-licensed under GPLv3 (see 38c49490)

## 1.0.0 (2015-07-12)

* `basic_unittest` is uses as unittest framework (see b5702dd2)

* Re-licensed under GPLv3 (see 19286aea)

## 0.9.1 (2014-01-07)

* Doxygen pipeline added (see 5e09b417bcc)

## 0.9.0 (2014-01-07)

* Initial version
