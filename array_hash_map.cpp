/*
 * This file is part of array_hash_map.
 *
 * array_hash_map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \file
 *    array_hash_map.cpp
 * \brief
 *    Implements an array hash map- implementation
 * \author
 *    Stefan Schweter
 *    Chris Vaszauskas and Tyler Richard
 * \note
 *    Idea of array hash has been described here:
 *       Askitis, N. & Zobel, J. (2005),
 *       Cache-conscious collision resolution for string hash tables,
 *       in ‘Proc. SPIRE String Processing and Information Retrieval Symp.’,
 *       Springer-Verlag, pp. 92–104
 *
 *    Based on the Chris Vaszauskas and Tyler Richard implementation:
 *
 *       https://github.com/chris-vaszauskas/hat-trie
 *
 *    Additions that were made to the implementation above:
 *        - key - value data structure
 *        - dereference operator returns std::pair<>
 */

#include "array_hash_map.hpp"
#include "array_hash_map_iterator.hpp"
#include "iterator"
#include <cstring>
#include <fstream>
#include <cassert>
#include <iostream>

array_hash_map::array_hash_map() : size(0), data(nullptr) { this->init(); }

array_hash_map::~array_hash_map() { this->destroy(); }

array_hash_map &array_hash_map::operator=(const array_hash_map &ah) {
  if (this != &ah) {
    this->size = ah.size;

    if (this->data) {
      this->destroy();
    }

    this->data = new char *[SLOT_SIZE];
    for (int i = 0; i < SLOT_SIZE; ++i) {
      if (ah.data[i]) {
        size_t space = *ah.data[i];
        this->data[i] = new char[space];
        memcpy(this->data[i], ah.data[i], space);
      } else {
        this->data[i] = nullptr;
      }
    }
  }
  return *this;
}

bool array_hash_map::exists(const char *str) const {
  uint16_t length;
  char *p = this->data[this->hash(str, length)];

  if (p == nullptr) {
    return false;
  }
  uint32_t s;
  return this->search(str, p, length, s) != nullptr;
}

bool array_hash_map::exists(const std::string &str) const {
  return exists(str.c_str());
}

size_t array_hash_map::get_size() const { return this->size; }

bool array_hash_map::empty() const { return this->get_size() == 0; }

bool array_hash_map::insert(const char *key, uint64_t value) {
  uint16_t length;
  int slot = this->hash(key, length);
  char *p = this->data[slot];
  if (p) {
    uint32_t occupied;
    if (this->search(key, p, length, occupied) != nullptr) {
      return false;
    }

    uint32_t current = *((uint32_t *)(p));
    uint32_t required = occupied + sizeof(uint16_t) + sizeof(uint64_t) + length;
    if (required > current) {
      this->grow_slot(slot, current, required);
    }

    p = this->data[slot] + occupied - sizeof(uint16_t);

  } else {
    uint32_t required =
        sizeof(uint32_t) + 2 * sizeof(uint16_t) + sizeof(uint64_t) + length;
    this->grow_slot(slot, 0, required);

    p = this->data[slot] + sizeof(uint32_t);
  }

  this->append_string(key, p, length, value);
  ++this->size;
  return true;
}

bool array_hash_map::insert(const std::string &key, uint64_t value) {
  return this->insert(key.c_str(), value);
}

void array_hash_map::clear() {
  this->destroy();
  this->init();
}

void array_hash_map::swap(array_hash_map &ah) {
  std::swap(this->data, ah.data);
  std::swap(this->size, ah.size);
}

array_hash_map_iterator array_hash_map::begin() const {
  array_hash_map_iterator result;
  if (this->get_size() == 0) {
    result = this->end();
  } else {
    result.data = this->data;
    while (result.data[result.slot] == nullptr) {
      ++result.slot;
    }
    result.p = result.data[result.slot] + sizeof(uint32_t);
  }
  result.slot_count = SLOT_SIZE;
  return result;
}

array_hash_map_iterator array_hash_map::end() const {
  return array_hash_map_iterator(SLOT_SIZE, nullptr, this->data, SLOT_SIZE);
}

array_hash_map_iterator array_hash_map::find(const char *str) const {
  uint16_t length;

  int slot = this->hash(str, length);
  char *p = this->data[slot];

  if (p == nullptr) {

    return this->end();
  }

  uint32_t occupied;

  p = this->search(str, p, length, occupied);

  return array_hash_map_iterator(slot, p, this->data, SLOT_SIZE);
}

array_hash_map_iterator array_hash_map::find(const std::string &str) const {
  return this->find(str.c_str());
}

bool array_hash_map::operator==(const array_hash_map &ah) {
  if (this->get_size() == ah.get_size()) {
    array_hash_map_iterator me = this->begin();
    array_hash_map_iterator them = ah.begin();
    array_hash_map_iterator stop = this->end();
    while (me != stop) {
      if ((strcmp((*me).first, (*them).first) == 0) &&
          ((*me).second == (*them).second)) {
        ++me;
        ++them;
      } else {
        return false;
      }
    }
    return true;
  }
  return false;
}

bool array_hash_map::operator!=(const array_hash_map &ah) {
  return !operator==(ah);
}

uint64_t &array_hash_map::operator[](const char *key) {
  /* This code is not very elegant. libstdc++ map just uses a few lines
   * and calls a special emplace function for storing an updated value
   * See:
   * http://gcc.gnu.org/viewcvs/gcc/trunk/libstdc%2B%2B-v3/include/bits/stl_map.h?revision=199924&view=markup
   * Maybe we can implement this later!
   */
  uint16_t length;

  /* Will be incremented later! */
  uint64_t default_value = 0;

  char *p = this->data[this->hash(key, length)];

  if (p == nullptr) {
    this->insert(key, default_value);
    return this->operator[](key);
  }

  p += sizeof(uint32_t);

  uint16_t w = *((uint16_t *)p);

  while (w != 0) {
    p += sizeof(uint16_t);
    if (w == length) {
      if (strcmp(key, p) == 0) {
        return *(uint64_t *)(p + w);
      }
    }
    p += w;

    /* move right */
    p += sizeof(uint64_t);

    w = *((uint16_t *)p);
  }

  this->insert(key, default_value);
  return this->operator[](key);
}

void array_hash_map::init() {
  this->data = new char *[SLOT_SIZE];
  memset(this->data, 0, SLOT_SIZE * sizeof(char *));
  this->size = 0;
}

void array_hash_map::destroy() {
  for (int i = 0; i < SLOT_SIZE; ++i) {
    delete[] this->data[i];
  }
  delete[] this->data;
  this->data = nullptr;
}

char *array_hash_map::search(const char *str, char *p, uint16_t length,
                             uint32_t &occupied) const {
  occupied = -1;
  char *start = p;

  p += sizeof(uint32_t);
  uint16_t w = *((uint16_t *)p);
  while (w != 0) {
    p += sizeof(uint16_t);
    if (w == length) {
      /* We do not need a strncmp function here, because
       * we already know, that the string in slot has the
       * same length as the string to be searched
       * -> strcmp is faster in this case than strncmp!
       */
      if (strcmp(str, p) == 0) {
        return p - sizeof(uint16_t);
      }
    }
    p += w;

    /* move right */
    p += sizeof(uint64_t);

    w = *((uint16_t *)p);
  }
  occupied = p - start + 1 * sizeof(uint16_t);
  return nullptr;
}

void array_hash_map::grow_slot(int slot, uint32_t current, uint32_t required) {
  uint32_t new_size = current;
  while (new_size < required) {
    new_size += 64;
  }

  char *p = this->data[slot];
  this->data[slot] = new char[new_size];
  if (p != nullptr) {
    memcpy(this->data[slot], p, current);
    delete[] p;
  }
  *((uint32_t *)(this->data[slot])) = new_size;
}

void array_hash_map::append_string(const char *key, char *p, uint16_t length,
                                   uint64_t value) {
  memcpy(p, &length, sizeof(uint16_t));
  p += sizeof(uint16_t);
  memcpy(p, key, length);
  p += length;

  memcpy(p, &value, sizeof(uint64_t));

  p += sizeof(uint64_t);

  length = 0;
  memcpy(p, &length, sizeof(uint16_t));
}

void array_hash_map::show_slot_distribution() {

  for (auto slot = 0; slot < SLOT_SIZE; ++slot) {

    std::cout << "Statistics for slot: " << slot << std::endl;

    auto p = this->data[slot];

    if (p != nullptr) {

      /* Show length of slot */
      uint32_t slot_length = *((uint32_t *)(p));

      p += sizeof(uint32_t);

      p += *((uint16_t *)p) + sizeof(uint16_t) + sizeof(uint64_t);

      uint64_t counter = 1;

      while (*((uint16_t *)p) != 0) {
        p += *((uint16_t *)p) + sizeof(uint16_t) + sizeof(uint64_t);
        ++counter;
      }

      std::cout << "Slot length " << slot_length << std::endl;
      std::cout << "Words in slot " << counter << std::endl;
    } else {
      std::cout << "Slot is empty!" << std::endl;
    }
  }
}

size_t array_hash_map::get_used_slots() {
  size_t used_slots = 0;

  for (auto slot = 0; slot < SLOT_SIZE; ++slot) {
    auto p = this->data[slot];

    if (p != nullptr) {
      /* A created, filled slot was found */
      ++used_slots;
    }
  }

  return used_slots;
}
