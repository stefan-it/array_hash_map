/*
 * This file is part of array_hash_map.
 *
 * array_hash_map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \file
 *    array_hash_map_iterator.cpp
 * \brief
 *    Implements an array hash map- implementation
 * \author
 *    Stefan Schweter
 *    Chris Vaszauskas and Tyler Richard
 * \note
 *    Idea of array hash has been described here:
 *       Askitis, N. & Zobel, J. (2005),
 *       Cache-conscious collision resolution for string hash tables,
 *       in ‘Proc. SPIRE String Processing and Information Retrieval Symp.’,
 *       Springer-Verlag, pp. 92–104
 *
 *    Based on the Chris Vaszauskas and Tyler Richard implementation:
 *
 *       https://github.com/chris-vaszauskas/hat-trie
 *
 *    Additions that were made to the implementation above:
 *        - key - value data structure
 *        - dereference operator returns std::pair<>
 */

#include "array_hash_map_iterator.hpp"

array_hash_map_iterator::array_hash_map_iterator()
    : slot(0), p(nullptr), data(nullptr), slot_count(0) {}

array_hash_map_iterator::array_hash_map_iterator(int slot, char *p, char **data,
                                                 int slot_count)
    : slot(slot), p(p), data(data), slot_count(slot_count) {}

array_hash_map_iterator &array_hash_map_iterator::operator++() {
  if (this->p) {
    this->p += *((uint16_t *)this->p) + sizeof(uint16_t) + sizeof(uint64_t);
    if (*((uint16_t *)this->p) == 0) {
      ++this->slot;
      while (this->slot < this->slot_count &&
             this->data[this->slot] == nullptr) {
        ++this->slot;
      }
      if (this->slot == this->slot_count) {
        this->p = nullptr;
      } else {
        this->p = this->data[this->slot] + sizeof(uint32_t);
      }
    }
  }

  return *this;
}

array_hash_map_iterator array_hash_map_iterator::operator++(int) {
  array_hash_map_iterator result = *this;
  operator++();
  return result;
}

std::pair<const char *, uint64_t> array_hash_map_iterator::operator*() const {
  if (this->p) {
    const char *key = this->p + sizeof(uint16_t);

    /*
     * Moves
     * length of string (uint16_t) cells
     * and one uint16_t cell
     * to "right"
     * dereference the found uint16_t cell -> value
     *
     */
    uint64_t value =
        *(uint64_t *)(this->p + sizeof(uint16_t) + *((uint16_t *)this->p));

    return std::make_pair(key, value);
  }

  return std::make_pair("error", 0);
}

bool array_hash_map_iterator::operator==(const array_hash_map_iterator &rhs) {
  return this->p == rhs.p;
}

bool array_hash_map_iterator::operator!=(const array_hash_map_iterator &rhs) {
  return !operator==(rhs);
}
