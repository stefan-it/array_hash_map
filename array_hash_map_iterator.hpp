/*
 * This file is part of array_hash_map.
 *
 * array_hash_map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \file
 *    array_hash_map_iterator.hpp
 * \brief
 *    Implements an array hash map- header
 * \author
 *    Stefan Schweter
 *    Chris Vaszauskas and Tyler Richard
 * \note
 *    Idea of array hash has been described here:
 *       Askitis, N. & Zobel, J. (2005),
 *       Cache-conscious collision resolution for string hash tables,
 *       in ‘Proc. SPIRE String Processing and Information Retrieval Symp.’,
 *       Springer-Verlag, pp. 92–104
 *
 *    Based on the Chris Vaszauskas and Tyler Richard implementation:
 *
 *       https://github.com/chris-vaszauskas/hat-trie
 *
 *    Additions that were made to the implementation above:
 *        - key - value data structure
 *        - dereference operator returns std::pair<>
 */

#ifndef array_hash_map_iterator_HPP
#define array_hash_map_iterator_HPP

#include <utility>
#include <iterator>

#ifdef __clang__
#include <cstdint>
#endif

class array_hash_map_iterator
    : public std::iterator<std::bidirectional_iterator_tag,
                           std::pair<const char *, uint64_t> > {
public:
  array_hash_map_iterator();

  /**
   * @brief Move this iterator forward to the next element in the table
   * Worst case O(n) where n = SLOT_SIZE
   * Calling this function on an end() iterator does nothing
   * @return array_hash_map_iterator Self-reference
   */
  array_hash_map_iterator &operator++();

  /**
   * @brief Postfix increment operator
   * Worst case O(n) where n = SLOT_SIZE
   */
  array_hash_map_iterator operator++(int);

  /**
   * @brief Iterator dereference operator
   * TODO: Also implement this routine as a pointer variant
   * O(1)
   * @return std::pair<const char, uint16_t>
   *         const char Key pointer to the string this iterator
   *                    points to
   *         uint16_t   Value pointer to the value this iterator points to
   */
  std::pair<const char *, uint64_t> operator*() const;

  /**
   * @brief Standard equality operator
   * O(1)
   */
  bool operator==(const array_hash_map_iterator &it);

  /**
   * @brief Standard inequality operator
   * O(1)
   */
  bool operator!=(const array_hash_map_iterator &it);

  typedef const char *reference;

  int slot;

  char *p;

  char **data;

  int slot_count;

  array_hash_map_iterator(int slot, char *p, char **data, int slot_count);

  typedef uint32_t size_type;
};

#endif
