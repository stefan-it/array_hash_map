/*
 * This file is part of array_hash_map.
 *
 * array_hash_map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \file
 *    array_hash_map_main.cpp
 * \brief
 *    Array hash map main method
 * \author
 *    Stefan Schweter <stefan@schweter.it> (2015)
 *
 */

#include <memory>
#include <iostream>
#include <algorithm>
#include "array_hash_map.hpp"

auto main() -> int {
  std::unique_ptr<array_hash_map> array(new array_hash_map);

  array->insert("Testvalue", 1);
  array->insert("Anotherone", 2);

  std::for_each(array->begin(), array->end(),
                [](const std::pair<const char *, long unsigned int> i) {
                  std::cout << i.first << " => " << i.second << std::endl;
                });
  return 0;
}
