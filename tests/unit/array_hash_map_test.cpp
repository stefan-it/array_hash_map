/*!
 * \file
 *    array_hash_map_test.cpp
 * \brief
 *    Array hash map unittests
 * \author
 *    Stefan Schweter <stefan@schweter.it> (2015)
 *
 */

#include <memory>
#include <iostream>
#include <algorithm>
#include <limits>
#include "../../array_hash_map.hpp"
#include "../basic_unittest/basic_unittest.hpp"

auto main() -> int {
  BEGIN_TEST_SUITE("Array Hash Map test suite");

    BEGIN_TEST("Clean");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      for (int i = 0; i < 100; ++i) {
        std::string new_word("Stefan");
        new_word += i;
        array->insert(new_word, i);
      }

      EXPECT_EQUAL(array->get_size(), 100);

      array->clear();

      EXPECT_EQUAL(array->get_size(), 0);
    }
    END_TEST();

    BEGIN_TEST("insertion_count");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Testvalue", 1);
      array->insert("Anotherone", 2);

      EXPECT_EQUAL(array->get_size(), 2);
    }
    END_TEST();

    BEGIN_TEST("insertion_exists");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Testvalue", 5);
      array->insert("Anotherone", 10);

      EXPECT_EQUAL(array->exists("Testvalue"), true);
      EXPECT_EQUAL(array->exists("Anotherone"), true);
    }
    END_TEST();

    BEGIN_TEST("insertion_not_exists");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Testvalue", 5);

      EXPECT_EQUAL(array->exists("Stefan"), false);
    }
    END_TEST();

    BEGIN_TEST("insertion_iterator");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Testvalue", 1337);

      auto it = array->begin();

      std::string first((*it).first);

      EXPECT_EQUAL(first, "Testvalue");
      EXPECT_EQUAL((*it).second, 1337);
    }
    END_TEST();

    BEGIN_TEST("insertion_iterator_find");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Stefan", 42);

      auto it = array->find("Stefan");

      std::string first((*it).first);

      EXPECT_EQUAL(first, "Stefan");
      EXPECT_EQUAL((*it).second, 42);
    }
    END_TEST();

    BEGIN_TEST("insertion_iterator_find_error");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Stefan", 42);

      auto it = array->find("Stef");

      std::string first((*it).first);

      EXPECT_EQUAL(first, "error");
      EXPECT_EQUAL((*it).second, 0);
    }
    END_TEST();

    BEGIN_TEST("insertion_iterator_find_ends");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Stefan", 42);

      auto it = array->find("Peters");

      bool is_end_iterator = false;

      if (it == array->end()) {
        is_end_iterator = true;
      }

      EXPECT_EQUAL(is_end_iterator, true);
    }
    END_TEST();

    BEGIN_TEST("empty");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);
      EXPECT_EQUAL(array->empty(), true);
    }
    END_TEST();

    BEGIN_TEST("not_empty");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Stefan", 1338);

      EXPECT_EQUAL(array->empty(), false);
    }
    END_TEST();

    BEGIN_TEST("insertion_unsigned_int_max_iterator");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("UINTMax", std::numeric_limits<unsigned long long int>::max());

      auto it = array->begin();

      std::string first((*it).first);

      EXPECT_EQUAL(first, "UINTMax");
      EXPECT_EQUAL((*it).second,
                   std::numeric_limits<unsigned long long int>::max());
    }
    END_TEST();

    BEGIN_TEST("insertion_huge_values_exists");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("UINTMax", std::numeric_limits<unsigned long long int>::max());

      array->insert("UINTMax2",
                    std::numeric_limits<unsigned long long int>::max() / 100);

      auto it1 = array->find("UINTMax");
      auto it2 = array->find("UINTMax2");

      std::string first1((*it1).first);
      std::string first2((*it2).first);

      EXPECT_EQUAL(first1, "UINTMax");
      EXPECT_EQUAL((*it1).second,
                   std::numeric_limits<unsigned long long int>::max());

      EXPECT_EQUAL(first2, "UINTMax2");
      EXPECT_EQUAL((*it2).second,
                   std::numeric_limits<unsigned long long int>::max() / 100);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      (*array)["Stefan"] = 1337;
      (*array)["Test"] = 42;

      EXPECT_EQUAL((*array)["Stefan"], 1337);
      EXPECT_EQUAL((*array)["Test"], 42);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator2");
    {
      array_hash_map array;

      (array)["Stefan"] = 1337;
      (array)["Test"] = 42;

      EXPECT_EQUAL((array)["Stefan"], 1337);
      EXPECT_EQUAL((array)["Test"], 42);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator_increment");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      (*array)["Stefan"] = 1336;
      (*array)["Test"] = 41;

      (*array)["Stefan"]++;
      (*array)["Test"]++;

      EXPECT_EQUAL((*array)["Stefan"], 1337);
      EXPECT_EQUAL((*array)["Test"], 42);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator_increment2");
    {
      array_hash_map array;

      (array)["Stefan"] = 1336;
      (array)["Test"] = 41;

      (array)["Stefan"]++;
      (array)["Test"]++;

      EXPECT_EQUAL((array)["Stefan"], 1337);
      EXPECT_EQUAL((array)["Test"], 42);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator_increment3");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      (*array)["Stefan"]++;

      EXPECT_EQUAL((*array)["Stefan"], 1);
    }
    END_TEST();

    BEGIN_TEST("insertion_subscript_operator_increment4");
    {
      array_hash_map array;

      (array)["Stefan"]++;

      EXPECT_EQUAL((array)["Stefan"], 1);
    }
    END_TEST();

    BEGIN_TEST("copy_assign");
    {
      std::shared_ptr<array_hash_map> array(new array_hash_map);

      std::shared_ptr<array_hash_map> array2(new array_hash_map);

      (*array)["Stefan"]++;

      array2 = array;

      EXPECT_EQUAL((*array2)["Stefan"], 1);
    }
    END_TEST();

    BEGIN_TEST("copy_assign2");
    {
      std::shared_ptr<array_hash_map> array(new array_hash_map);

      std::string test_word = "Stefan";

      for (int i = 0; i < 100; ++i) {
        std::string temp_word(test_word);
        temp_word += i;
        array->insert(temp_word, i);
      }

      std::shared_ptr<array_hash_map> array2 = array;

      EXPECT_EQUAL(array2->get_size(), 100);
    }
    END_TEST();

    BEGIN_TEST("move_assign");
    {
      std::unique_ptr<array_hash_map> array(new array_hash_map);

      array->insert("Stefan", 42);

      std::unique_ptr<array_hash_map> array2 = std::move(array);

      EXPECT_EQUAL(array2->get_size(), 1);
    }
    END_TEST();

    BEGIN_TEST("equals");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("Stefan", 1338);

      bool array_equals = false;

      array_equals = (array == array2);

      EXPECT_EQUAL(array_equals, true);
    }
    END_TEST();

    BEGIN_TEST("not_equals");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("Stefan", 1339);

      bool array_equals = true;

      array_equals = (array == array2);

      EXPECT_EQUAL(array_equals, false);
    }
    END_TEST();

    BEGIN_TEST("not_equals2");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("TotallyAnotherValue", 1338);

      bool array_equals = true;

      array_equals = (array == array2);

      EXPECT_EQUAL(array_equals, false);
    }
    END_TEST();

    BEGIN_TEST("not_equals3");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("AnotherKeyAndValue", 1337);

      bool array_equals = true;

      array_equals = (array == array2);

      EXPECT_EQUAL(array_equals, false);
    }
    END_TEST();

    BEGIN_TEST("not_equals4");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("Stefan", 1337);

      bool array_not_equals = false;

      array_not_equals = (array != array2);

      EXPECT_EQUAL(array_not_equals, true);
    }
    END_TEST();

    BEGIN_TEST("not_equals5");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("AnotherValueButSameKey", 1338);

      bool array_not_equals = false;

      array_not_equals = (array != array2);

      EXPECT_EQUAL(array_not_equals, true);
    }
    END_TEST();

    BEGIN_TEST("not_equals6");
    {
      array_hash_map array;

      array.insert("Stefan", 1338);

      array_hash_map array2;

      array2.insert("AnotherKeyAndValue", 1339);

      bool array_not_equals = false;

      array_not_equals = (array != array2);

      EXPECT_EQUAL(array_not_equals, true);
    }
    END_TEST();

  END_TEST_SUITE();

  return 0;
}
