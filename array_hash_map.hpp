/*
 * This file is part of array_hash_map.
 *
 * array_hash_map is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * \file
 *    array_hash_map.hpp
 * \brief
 *    Implements an array hash map- header
 * \author
 *    Stefan Schweter
 *    Chris Vaszauskas and Tyler Richard
 * \note
 *    Idea of array hash has been described here:
 *       Askitis, N. & Zobel, J. (2005),
 *       Cache-conscious collision resolution for string hash tables,
 *       in ‘Proc. SPIRE String Processing and Information Retrieval Symp.’,
 *       Springer-Verlag, pp. 92–104
 *
 *    Based on the Chris Vaszauskas and Tyler Richard implementation:
 *
 *       https://github.com/chris-vaszauskas/hat-trie
 *
 *    Additions that were made to the implementation above:
 *        - key - value data structure
 *        - dereference operator returns std::pair<>
 */

#include "array_hash_map_iterator.hpp"

#ifndef ARRAY_HASH_HPP
#define ARRAY_HASH_HPP

#define SLOT_SIZE 524288

// 8388608 2^23
// 8349696
// 4194304 2^22
// 2097152 2^21
// 1048576 2^20
// 524288  2^19

class array_hash_map {
public:
  /**
   * @brief Default constructor.
   * O(1)
   */
  array_hash_map();

  /**
   * @brief Copy constructor.
   * O(n) where n = SLOT_SIZE
  */
  array_hash_map(const array_hash_map &ah);

  /**
   * @brief Standard destructor.
   */
  ~array_hash_map();

  /**
   * @brief Assignment operator.
   * O(n) where n = SLOT_SIZE
   */
  array_hash_map &operator=(const array_hash_map &ah);

  /**
   * @brief Determines whether a str is in the table.
   * @param str String to search for
   * @return bool True, if str is in the table
   */
  bool exists(const char *str) const;

  /**
   * @see exists(const char *str)
   */
  bool exists(const std::string &str) const;

  /**
   * @brief Gets the number of elements in the table.
   * O(1)
   * @return size_t Number of elements
   */
  size_t get_size() const;

  /**
   * @brief Determines whether the table is empty.
   * O(1)
   * @return bool True, is table is empty
   */
  bool empty() const;

  /**
   * @brief Inserts str into the table.
   * O(m) where m is the length of str
   * @param str String to insert
   * @return bool True if str is successfully inserted
   *         bool False, if str already appears in the table
   */
  bool insert(const char *key, uint64_t value);

  /**
   * @see insert(const char *str)
   */
  bool insert(const std::string &key, uint64_t value);

  /**
   * @brief Clears all the elements from the hash table.
   * O(n) where n = SLOT_SIZE
   */
  void clear();

  /**
   * @brief Swaps information between two array hashes
   * O(1)
   * @param ah Array Hash
   */
  void swap(array_hash_map &ah);

  /**
   * @brief Gets an iterator to the first element in the table
   * O(n) where n = SLOT_SIZE
   */
  array_hash_map_iterator begin() const;

  /**
   * @brief Gets an iterator to one past the last element in the hash table
   * O(1)
   */
  array_hash_map_iterator end() const;

  /**
   * @brief Searches for str in the table
   * O(m) where m is the length of str
   * @param str String to search for
   * @return array_hash_map_iterator Iterator to str in the table,
   *                        or end() if @a str is not in the table
   */
  array_hash_map_iterator find(const char *str) const;

  /**
   * @see find(const char *str) const
   */
  array_hash_map_iterator find(const std::string &str) const;

  /**
   * @brief Shows slot distribution
   * This function shows slot distribution = length of slot + count of
   * words in a slot
   */
  void show_slot_distribution();

  /**
   * @brief Equality operator
   * O(n) where n = size()
   * @param ah
   * @return bool True, when equal
   *         bool False, when not equal
   */
  bool operator==(const array_hash_map &ah);

  /**
   * @brief Inequality operator
   * O(n) where n = size()
   * @param ah
   * @return bool True, when not equal
   *         bool False, when equal
   */
  bool operator!=(const array_hash_map &ah);

  /**
   * @brief Implements an array subscript operator
   * If key does not exists, it will be inserted (value = 1)
   * Value can be updated via "=" operator
   * Notice: EXPERIMENTAL implementation!
   * @param key Key to be inserted or updated
   */
  uint64_t &operator[](const char *key);

private:
  /**
   * @brief Initializes the internal data pointers
   */
  void init();

  /**
   * @brief Clears all the memory used by the table
   */
  void destroy();

  /**
   * @brief Hashes str to an integer, its slot in the hash table
   * We use the Zobel hash function
   * @param str String to hash
   * @param length Length of str. Calculated as this function runs
   * @param seed Seed for the hash function
   * @return int Hashed value of str (its slot in the table)
   */
  int hash(const char *str, uint16_t &length, int seed = 23) const;

  /**
   * @brief Searches for @a str in the table
   * @param str String to search for
   * @param length Length of str
   * @param p Slot in data that str goes into
   * @param occupied Number of bytes in the slot that are currently in use.
   *                 This value is only meaningful when this function
   *                 doesn't find str
   * @return char If str is found in the table, returns a pointer to
   *              the string and its corresponding length.
   *              If not, returns NULL.
   */
  char *search(const char *str, char *p, uint16_t length,
               uint32_t &occupied) const;

  /**
   * @brief Increases the capacity of a slot to be >= required
   * @param slot Slot to change
   * @param current Current size of the slot
   * @param required Required size of the slot
   */
  void grow_slot(int slot, uint32_t current, uint32_t required);

  /**
   * @brief Appends a string to a list of strings in a slot
   * Assumes the slot is big enough to hold the string
   * @param str String to append
   * @param p Pointer to the location in the slot this string
   *          should occupy
   * @param length Length of str
   */
  void append_string(const char *key, char *p, uint16_t length, uint64_t value);

  size_t size;

  char **data;

  size_t get_used_slots();
};

inline int array_hash_map::hash(const char *str, uint16_t &length,
                                int seed) const {
  int h = seed;
  length = 0;

  while (*str != '\0') {
    h = h ^ ((h << 5) + (h >> 2) + *str);

    ++length;
    ++str;
  }

  ++length;

  return h & (SLOT_SIZE - 1);
}

#endif
