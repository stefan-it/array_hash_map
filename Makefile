SHELL = /bin/bash
MAKESHELL = /bin/bash

CXX ?= g++
CXXFLAGS ?= -std=c++11 -Ofast -Wall -Wextra -Weffc++ -Wpedantic \
            -fstack-protector-strong

ADDRESS_SANITIZER_COMPILING =
ADDRESS_SANITIZER_LINKING   =

USE_ADDRESS_SANITIZER ?= 0

ifeq ($(USE_ADDRESS_SANITIZER),1)
ADDRESS_SANITIZER_COMPILING = -g -fsanitize=address -fno-omit-frame-pointer
ADDRESS_SANITIZER_LINKING   = -g -fsanitize=address
endif

GIT ?= git
BASIC_UNITTEST_GIT_ADDRESS ?= https://gitlab.com/stefan-it/basic_unittest.git

EXEC ?= array_hash_map_example
TEST_EXEC ?= array_hash_map_tests
SRCS = $(wildcard *.cpp)
OBJS = $(SRCS:.cpp=.o)

TEST_SRCS  = $(filter-out array_hash_map_main.cpp, $(SRCS))
TEST_SRCS += $(wildcard tests/unit/*.cpp)
TEST_OBJS  = $(TEST_SRCS:.cpp=.o)

CLEANUP  = $(patsubst %, %.cleanup, $(shell find -type f -iname \*.o))
CLEANUP += $(patsubst %, %.cleanup, $(shell find -type f -name $(EXEC)))
CLEANUP += $(patsubst %, %.cleanup, $(shell find -type f -name $(TEST_EXEC)))

SILENT ?= @
QUIET ?= 2>&1 >/dev/null

.PHONY: default
default: all

.PHONY: all
all: bin tests

.PHONY: bin
bin: $(EXEC)

.PHONY: tests
tests: tests/basic_unittest/.git
	+$(SILENT) $(MAKE) $(TEST_EXEC)

tests/basic_unittest/.git:
	$(GIT) clone $(BASIC_UNITTEST_GIT_ADDRESS) tests/basic_unittest

$(EXEC): $(OBJS)
	$(SILENT) $(CXX) $(ADDRESS_SANITIZER_LINKING) $(OBJS) -o $(EXEC)

$(TEST_EXEC): $(TEST_OBJS)
	$(SILENT) $(CXX) $(ADDRESS_SANITIZER_LINKING) $(TEST_OBJS) -o $(TEST_EXEC)

%.o: %.cpp
	$(SILENT) $(CXX) $(ADDRESS_SANITIZER_COMPILING) -c $(CXXFLAGS) $< -o $@

.PHONY: clean
clean: $(CLEANUP)

%.cleanup: %
	$(SILENT) $(RM) $<

.PHONY: distclean
distclean: clean
	$(SILENT) $(GIT) clean -fdqx -f
